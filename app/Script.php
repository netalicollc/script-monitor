<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Script extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scripts';
}
