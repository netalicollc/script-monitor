<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class Scripts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scripts:master';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    public function isNewRelic($node) {
      if (strpos($node->text(), 'window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey"') !== false) {
        return true;
      }
    }

    public function containsFormKey($node) {
      if (strpos($node->text(), 'form_key') !== false) {
        return true;
      }

      if (strpos($node->text(), 'formKey') !== false) {
        return true;
      }
    }

    public function containsRiskified($node) {
      if (strpos($node->text(), 'riskifiedBeaconLoad()') !== false) {
        return true;
      }
    }

    public function isMsrpClick($node) {
      if (strpos($node->text(), 'msrp-click-') !== false) {
        return true;
      }
    }

    public function stripFormKey($contents) {
      $contentsClean = str_replace("{{", "", $contents);
      $contentsClean = str_replace("}}", "", $contentsClean);

      $crawler = new Crawler($contentsClean);
      var_dump($contentsClean);

      $crawler->each(function (Crawler $node, $i) {
        echo $node->text();
      });


      return $contents;
    }

    public function isHaveToBeIgnored($node) {
      if ($this->isNewRelic($node)) {
        return true;
      }

      if ($this->containsFormKey($node)) {
        return true;
      }

      if ($this->containsRiskified($node)) {
        return true;
      }

      if ($this->isMsrpClick($node)) {
        return true;
      }


    }
}
