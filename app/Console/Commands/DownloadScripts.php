<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class DownloadScripts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scripts:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sites = \App\Site::all();
        foreach ($sites as $site) {
          $crawler = \Goutte::request('GET', $site->url);
          $crawler->filter('script')->each( function ($node) use ($site) {
            $contents = $node->text();
            $src = $node->attr('src');

            $script = new \App\Script;
            $script->site_id = $site->id;
            $script->src = $src;
            $script->contents = preg_replace('/\s+/', '', $contents);
            $script->save();
          });
        }
    }
}
