<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckScripts extends Scripts
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scripts:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $sites = \App\Site::all();
      foreach ($sites as $site) {
        $siteChanged = false;
        echo "Checking " . $site->name . "....\n";
        $crawler = \Goutte::request('GET', $site->url);
        $crawler->filter('script')->each( function ($node) use ($site) {
          $contents = $node->text();
          $src = $node->attr('src');

          if (!$this->isHaveToBeIgnored($node)) {
            $script = \App\Script::where('contents', preg_replace('/\s+/', '', $contents))->where('src', $src)->where('site_id', $site->id)->first();
            if (!$script) {
              echo "New Script: \n";
              echo "src: " . $src . "\n";
              echo "contents: " . preg_replace('/\s+/', '', $contents);
              echo "\n";
              $siteChanged = true;
            } else {
              // echo "Old Script: \n";
              // echo "src: " . $src . "\n";
              // echo "contents: " . preg_replace('/\s+/', '', $contents);
            }
          }
        });
        // if (!$siteChanged) {
        //   echo "No Changes on " . $site->name . "....\n";
        // } else {
        //   echo "CHANGES DETECTED ON " . $site->name . "....\n";
        // }
      }
    }


}
