# Readme

Right now you have to manually insert sites into the sites database table.

If you’ve confirmed all your sites are in a clean state run this command to download all the current scripts:

 `php artisan scripts:download;`

When you want to see the changes since you last downloaded, run this:

`php artisan scripts:check;`

If everything looks clean you can reset the scripts in the database and re-download the latest changes:

`php artisan scripts:delete;php artisan scripts:download;`
